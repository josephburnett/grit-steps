package main

import "os"

func main() {
	tfDir, err := os.MkdirTemp("", "")
	if err != nil {
		panic(err)
	}
	stepRunnerOutputFile := os.Getenv("STEP_RUNNER_OUTPUT")
	err = os.WriteFile(stepRunnerOutputFile, []byte("tf_dir="+tfDir), 0660)
	if err != nil {
		panic(err)
	}
}
